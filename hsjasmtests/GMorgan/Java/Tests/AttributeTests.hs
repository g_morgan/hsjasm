{-# OPTIONS_GHC -F -pgmF htfpp #-}
module GMorgan.Java.Tests.AttributeTests where

import Data.Map hiding (foldl)
import Data.ByteString.Lazy hiding (foldl)
import qualified Data.ByteString.Lazy.Char8 as BLS

import GMorgan.Java.Class
import GMorgan.Java.Error
import GMorgan.Java.Parsing.ParseAttributes
import GMorgan.Java.AttributeInfo
import GMorgan.Java.Class.Binary.Tools
import GMorgan.Java.Binary.JUtf8

import Test.Framework

{-# ANN module "HLint: ignore" #-}

createConstantPool :: U2 -> [(U2, ConstantPoolEntry)] -> ConstantPool
createConstantPool len entries = ConstantPool len (foldl addConstant Data.Map.empty entries )
                                 where addConstant map (index, entry) = insert index entry map
                                 
constantPool1 = createConstantPool 1 [(1, IntegerInfo 76)]
constantPool2 = createConstantPool 4 [(1, LongInfo 0 72), (3, IntegerInfo 76), (4, Utf8Info (jutf8FromString "Hello_world"))]

test_badAttrConst = do let attrBytes = pack [0, 1, 0, 0, 0, 5, 1, 2, 3, 4, 5, 6, 7]
                       BadAttributeConstant msg <- assertLeftVerbose "This test should give an error" (parseAttribute constantPool1 attrBytes)
                       assertEqual "Expected a constant of type Utf8Info, actually got IntegerInfo" msg

test_shortAttr1 = do let attrBytes = pack [0]
                     ClassFileTooShort msg <- assertLeftVerbose "This test should give an error" (parseAttribute constantPool1 attrBytes)
                     assertEqual "Could not read attribute_name_index" msg

test_noAttrConst = do let attrBytes = pack [0, 2]
                      BadAttributeConstant msg <- assertLeftVerbose "This test should give an error" (parseAttribute constantPool2 attrBytes)
                      assertEqual "No constant with this index" msg

test_generalAttr = do let attrBytes = pack [0, 4, 0, 0, 0, 7, 1, 2, 3, 4, 5, 6, 7, 8, 9]
                      (attr, bs) <- assertRightVerbose "This test shouldn't give an error" (parseAttribute constantPool2 attrBytes)
                      genAttr <- assertRightVerbose "This test should give a GeneralAttribute" (getGeneralAttributeOrError attr)
                      assertEqual 4 (attributeNameIndex genAttr)
                      assertEqual 7 (attributeLength genAttr)
                      assertEqual 2 (BLS.length bs)

getGeneralAttributeOrError :: AttributeInfo -> Either AttributeError AttributeInfo
getGeneralAttributeOrError attr@(GeneralAttribute _ _ _) = Right attr
getGeneralAttributeOrError _ = Left WrongAttributeType

data AttributeError = 
  WrongAttributeType
  deriving Show