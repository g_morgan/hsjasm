{-# OPTIONS_GHC -F -pgmF htfpp #-}
module GMorgan.Java.Tests.InterfaceParseTests where
import GMorgan.Java.Class.Parse
import GMorgan.Java.Class
import GMorgan.Java.Error
import qualified Data.ByteString.Lazy as BL

import Test.Framework


{-# ANN module "HLint: ignore" #-}


test_interfaces1 :: IO ()
test_interfaces1 = do let interfaceBytes = BL.pack [0,1,0,6]
                          outputInterfaces = readInterfaces interfaceBytes
                      (cls, numInt, bs) <- assertRightVerbose "This test should not give an error" outputInterfaces
                      Interfaces num intList <- return cls
                      assertEqual num (fromIntegral (length intList))
                      assertEqual 6 (head intList)
                      assertEqual bs BL.empty
                      assertEqual numInt num

test_interfaces2 :: IO ()
test_interfaces2 = do let interfaceBytes = BL.pack [0,0]
                          outputInterfaces = readInterfaces interfaceBytes
                      (cls, numInt, bs) <- assertRightVerbose "This test should not give an error" outputInterfaces
                      Interfaces num intList <- return cls
                      assertEqual num (fromIntegral (length intList))
                      assertEqual [] intList
                      assertEqual bs BL.empty
                      assertEqual numInt num

test_interfaces3 :: IO ()
test_interfaces3 = do let interfaceBytes = BL.pack [0]
                          outputInterfaces = readInterfaces interfaceBytes
                      ClassFileTooShort msg <- assertLeftVerbose "This test should give an error" outputInterfaces
                      assertEqual "Could not read the interface count" msg

test_interfaces4 :: IO ()
test_interfaces4 = do let interfaceBytes = BL.pack [0, 1, 5]
                          outputInterfaces = readInterfaces interfaceBytes
                      ClassFileTooShort msg <- assertLeftVerbose "This test should give an error" outputInterfaces
                      assertEqual "Could not read the interfaces" msg

test_interfaces5 :: IO ()
test_interfaces5 = do let interfaceBytes = BL.pack [0, 4, 5, 3, 9, 2, 8, 0]
                          outputInterfaces = readInterfaces interfaceBytes
                      ClassFileTooShort msg <- assertLeftVerbose "This test should give an error" outputInterfaces
                      assertEqual "Could not read the interfaces" msg


