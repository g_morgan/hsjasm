{-# OPTIONS_GHC -F -pgmF htfpp #-}
module GMorgan.Java.Tests.MagicNumberParseTests where
import GMorgan.Java.Class.Parse
import GMorgan.Java.Class.ParseTypes
import qualified Data.ByteString.Lazy as BL

import Test.Framework


{-# ANN module "HLint: ignore" #-}

test_magicNumber1 = do let headerBytes = BL.pack [202,254,186,190] -- 0xCAFEBABE
                           status = ParseStatusOld headerBytes 0 []
                       (ParseStatusOld bs byteNum entries) <- assertRight (parseMagicNumber status)
                       assertEqual BL.empty bs
                       assertEqual 4 byteNum
                       assertEqual [] entries
