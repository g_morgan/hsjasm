{-# OPTIONS_GHC -F -pgmF htfpp #-}
module GMorgan.Java.Tests.FullClassTests where

import qualified Data.ByteString.Lazy as BL


import GMorgan.Java.Binary.JUtf8
import GMorgan.Java.Class.Parse
import GMorgan.Java.Class
import GMorgan.Java.Error
import GMorgan.Java.Class.Binary.Tools

import Test.Framework

{-# ANN module "HLint: ignore" #-}


javaClassHello = "Hello.class"
javaClassHelloName = jutf8FromString "Hello"

test_FullClass1 = do readResult <- readClass javaClassHello
                     clazz <- assertRight readResult
                     assertEqual 0 (minorVersion clazz)
                     assertEqual 51 (majorVersion clazz)
                     assertEqual 33 (accessFlags clazz)
                     assertEqual (fromIntegral (numInterfaces clazz)) (length (interfaces clazz))
                     assertEqual 5 (thisClass clazz)
                     thisClsConst <- assertRight (getClassInfoOrError (getConstant clazz (thisClass clazz)))
                     assertEqual 21 (nameIndex thisClsConst)
                     clsNameConst <- assertRight (getUtf8InfoOrError (getConstant clazz (nameIndex thisClsConst)))
                     assertEqual javaClassHelloName (content clsNameConst)
                     

data ConstantError =
  NoConstant |
  WrongConstant ConstantPoolEntry |
  ConstantError String
  deriving Show

getClassInfoOrError :: Maybe ConstantPoolEntry -> Either ConstantError ConstantPoolEntry
getClassInfoOrError (Just const@(ClassConstant _)) = Right const
getClassInfoOrError (Just const) = Left (WrongConstant const)
getClassInfoOrError Nothing = Left NoConstant

getUtf8InfoOrError :: Maybe ConstantPoolEntry -> Either ConstantError ConstantPoolEntry
getUtf8InfoOrError (Just const@(Utf8Info _)) = Right const
getUtf8InfoOrError (Just const) = Left (WrongConstant const)
getUtf8InfoOrError Nothing = Left NoConstant