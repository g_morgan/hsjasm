{-# OPTIONS_GHC -F -pgmF htfpp #-}
module Main where


import Test.Framework

import {-@ HTF_TESTS @-} GMorgan.Java.Tests.InterfaceParseTests
import {-@ HTF_TESTS @-} GMorgan.Java.Tests.MagicNumberParseTests
import {-@ HTF_TESTS @-} GMorgan.Java.Tests.FullClassTests
import {-@ HTF_TESTS @-} GMorgan.Java.Tests.AttributeTests

main :: IO()
main = htfMain htf_importedTests
