module GMorgan.Java.Error where

import GMorgan.Java.Class.Binary.Tools

data JavaError =
  WrongMagicNumber |
  ClassFileTooShort String |
  UnknownTag U1 |
  ConstantPoolLargerThanSpecifiedSize |
  BadAttributeConstant String
  deriving Show