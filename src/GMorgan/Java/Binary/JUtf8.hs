module GMorgan.Java.Binary.JUtf8 (JUtf8 (JUtf8), jutf8FromString, utf8Text,
                                  utf8Str, encode, decode) where

import Prelude hiding (length)
import Data.ByteString as SBS
import Data.Data
import Data.Bits
import Data.Text hiding (length)
import Data.Text.Encoding

import GMorgan.Java.Class.Binary.Tools

data JUtf8 =
  JUtf8
  {
    _utf8Text :: Text 
  }
  deriving (Eq, Typeable, Data)

utf8Text :: Functor f => (Text -> f Text) -> JUtf8 -> f JUtf8
utf8Text eltFn (JUtf8 oldText) = fmap JUtf8 (eltFn oldText)

utf8Str :: Functor f => (String -> f String) -> JUtf8 -> f JUtf8
utf8Str eltFn oldJutf8 = fmap jutf8FromString (eltFn . jUtf8ToString $ oldJutf8)

instance Show JUtf8 where
  show str = "(Length:" ++ show (len str) ++ ", Content:" ++ jUtf8ToString str ++ ")"

len :: JUtf8 -> U2
len (JUtf8 text) = fromIntegral . length . textToJutf8Bstr $ text

encode :: JUtf8 -> ByteString
encode (JUtf8 text) = textToJutf8Bstr text

decode :: ByteString -> JUtf8
decode = JUtf8 . jutf8BstrToText

jutf8FromString :: String -> JUtf8
jutf8FromString = JUtf8 . Data.Text.pack

jUtf8ToString :: JUtf8 -> String
jUtf8ToString (JUtf8 text) = Data.Text.unpack text

jutf8BstrToText :: ByteString -> Text
jutf8BstrToText = jutf8CharsToText . SBS.unpack

jutf8CharsToText :: [U1] -> Text
jutf8CharsToText = decodeUtf8 . SBS.pack . jutf8CharsToUtf8

textToJutf8Bstr :: Text -> ByteString
textToJutf8Bstr =  SBS.pack . textToJutf8Chars

textToJutf8Chars :: Text -> [U1]
textToJutf8Chars = utf8CharsToJutf8 . SBS.unpack . encodeUtf8

jutf8CharsToUtf8 :: [U1] -> [U1]
jutf8CharsToUtf8 [] = []
jutf8CharsToUtf8 (first:rest)
  | isOneByteChar first = first:jutf8CharsToUtf8 rest
  | isTwoByteChar first = let (second:rest2) = rest
                          in if isTwoByteNull first second
                             then 0:jutf8CharsToUtf8 rest2
                             else first:second:jutf8CharsToUtf8 rest2
  | otherwise = let (second:third:rest2) = rest
                in first:second:third:jutf8CharsToUtf8 rest2

utf8CharsToJutf8 :: [U1] -> [U1]
utf8CharsToJutf8 [] = []
utf8CharsToJutf8 (first:rest)
  | isOneByteChar first = if first == 0
                          then 192:128:utf8CharsToJutf8 rest
                          else first:utf8CharsToJutf8 rest
  | isTwoByteChar first = let (second:rest2) = rest
                          in first:second:utf8CharsToJutf8 rest2
  | otherwise = let (second:third:rest2) = rest
                in first:second:third:utf8CharsToJutf8 rest2

isTwoByteNull :: U1 -> U1 -> Bool
isTwoByteNull first second = (first == 192) && (second == 128)

isOneByteChar :: U1 -> Bool
isOneByteChar byte = (byte .&. 128) == 0

isTwoByteChar :: U1 -> Bool
isTwoByteChar byte = (byte .&. 224) == 192

