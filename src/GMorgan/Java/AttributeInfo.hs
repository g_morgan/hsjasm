module GMorgan.Java.AttributeInfo where

import GMorgan.Java.Class.Binary.Tools

data AttributeInfo =
  GeneralAttribute
  {
    attributeNameIndex  :: U2,
    attributeLength     :: U4,
    info                :: [U1]
  }
  deriving Show
