module GMorgan.Java.Class where

import Data.Map hiding (foldl)
import Data.Typeable
import Data.Data

import GMorgan.Java.Class.Binary.Tools
import qualified GMorgan.Java.Error as JE
import GMorgan.Java.Binary.JUtf8

data Class = 
  Class 
  {
    minorVersion :: U2,
    majorVersion :: U2,
    constantPool :: ConstantPool,
    accessFlags :: U2,
    thisClass :: U2,
    superClass :: U2,
    numInterfaces :: U2,
    interfaces :: [U2]
  }
  deriving Show

data ConstantPool =
  ConstantPool
  {
    poolLength :: U2,
    pool :: Map U2 ConstantPoolEntry
  }
  deriving (Show, Eq)

getConstant :: Class -> U2 -> Maybe ConstantPoolEntry
getConstant clazz index = getConstantFromPool index (constantPool clazz)

getConstantFromPool :: U2 -> ConstantPool -> Maybe ConstantPoolEntry
getConstantFromPool index cPool = Data.Map.lookup index (pool cPool)

verifyConstantMap :: Map U2 ConstantPoolEntry -> U2 -> Maybe JE.JavaError
verifyConstantMap constMap max = Nothing

data ConstantPoolEntry =
  ClassConstant 
  {
    nameIndex :: U2 
  } |
  FieldRef
  {
    classIndex :: U2,
    nameAndTypeIndex :: U2
  } |
  MethodRef 
  {
    classIndex :: U2,
    nameAndTypeIndex :: U2
  } |
  InterfaceMethodRef 
  {
    classIndex :: U2,
    nameAndTypeIndex :: U2
  } |
  StringInfo
  {
    stringIndex :: U2
  } |
  IntegerInfo
  {
    bytes :: U4
  } |
  FloatInfo
  {
    bytes :: U4
  } |
  LongInfo
  {
    highBytes :: U4,
    lowBytes :: U4
  } |
  DoubleInfo
  {
    highBytes :: U4,
    lowBytes :: U4
  } |
  NameAndTypeInfo
  {
    nameIndex :: U2,
    descriptorIndex :: U2
  } |
  Utf8Info
  {
    content :: JUtf8
  }
  deriving (Show, Eq, Typeable, Data)

tag :: ConstantPoolEntry -> U1
tag (ClassConstant _) = constantClass
tag (FieldRef _ _) = constantFieldRef
tag (MethodRef _ _) = constantMethodRef
tag (InterfaceMethodRef _ _) = constantInterfaceMethodRef
tag (StringInfo _) = constantStringInfo
tag (IntegerInfo _) = constantInteger
tag (FloatInfo _) = constantFloat
tag (LongInfo _ _) = constantLong
tag (DoubleInfo _ _) = constantDouble
tag (NameAndTypeInfo _ _) = constantNameAndType
tag (Utf8Info _) = constantUtf8

typeName :: ConstantPoolEntry -> String
typeName = show . toConstr

constantLength :: ConstantPoolEntry -> U2
constantLength (LongInfo _ _) = 2
constantLength (DoubleInfo _ _) = 2
constantLength _ = 1

data ClassEntry =
  Version
  { 
    ceMinorVersion :: U2,
    ceMajorVersion :: U2 
  } |
  CeConstantPool
  {
    ceConstantPool :: ConstantPool
  } |
  AccessFlags
  {
    flags :: U2
  } |
  ThisClass
  {
    this :: U2
  } |
  SuperClass
  {
    super :: U2
  } |
  Interfaces
  {
    ceNumInterfaces :: U2,
    ceInterfaces :: [U2]
  }
  deriving (Show, Eq)

emptyConstantMap :: Map U2 ConstantPoolEntry
emptyConstantMap = empty

emptyConstantPool :: ConstantPool
emptyConstantPool = ConstantPool {poolLength = 0, pool = emptyConstantMap}

emptyInterfaceList :: [U2]
emptyInterfaceList = []

emptyClass :: Class
emptyClass = Class 0 0 emptyConstantPool 0 0 0 0 emptyInterfaceList
  
createClass :: [ClassEntry] -> Class
createClass = mergeClassEntries emptyClass

mergeClassEntries :: Class -> [ClassEntry] -> Class
mergeClassEntries = foldl updateClass

updateClass :: Class -> ClassEntry -> Class
updateClass oldClass (Version {ceMinorVersion=minor, ceMajorVersion=major})
  = oldClass{ minorVersion = minor, majorVersion = major }
updateClass oldClass (CeConstantPool { ceConstantPool = pool })
  = oldClass{ constantPool = pool }
updateClass oldClass (AccessFlags {flags=flags})
  = oldClass{ accessFlags = flags  }
updateClass oldClass (ThisClass {this=this})
  = oldClass{ thisClass = this  }
updateClass oldClass (SuperClass {super=super})
  = oldClass{ superClass = super  }
updateClass oldClass (Interfaces {ceNumInterfaces = num, ceInterfaces=ints})
  = oldClass{ interfaces = ints, numInterfaces = num  }


classFormatMagicNumber :: U4
classFormatMagicNumber = 0xCAFEBABE

constantClass :: U1
constantClass = 7

constantFieldRef :: U1
constantFieldRef = 9

constantMethodRef :: U1
constantMethodRef = 10

constantInterfaceMethodRef :: U1
constantInterfaceMethodRef = 11

constantStringInfo :: U1
constantStringInfo = 8

constantInteger :: U1
constantInteger = 3

constantFloat :: U1
constantFloat = 4

constantLong :: U1
constantLong = 5

constantDouble :: U1
constantDouble = 6

constantNameAndType :: U1
constantNameAndType = 12

constantUtf8 :: U1
constantUtf8 = 1
