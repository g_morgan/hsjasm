module GMorgan.Java.Class.Parse where

import qualified Data.ByteString.Lazy            as BL
import qualified Data.Map                        as M
import qualified GMorgan.Java.Class              as JC
import qualified GMorgan.Java.Error              as JE
import GMorgan.Java.Class.ParseConstants
import GMorgan.Java.Class.ParseTypes
import GMorgan.Java.Class.Binary.Tools


getParseEntries :: ParseStatusOld -> [JC.ClassEntry]
getParseEntries (ParseStatusOld bs byteNum entries) = entries

readClass :: String -> IO (Either (JE.JavaError, ParseStatus) JC.Class)
readClass filename = do bytes <- BL.readFile filename
                        return (parseClass bytes)

parseClass :: BL.ByteString -> Either (JE.JavaError, ParseStatus) JC.Class
parseClass classContents = parseClass' (ParseStatus classContents 0 JC.emptyClass)

parseClass' :: ParseStatus -> Either (JE.JavaError, ParseStatus) JC.Class
parseClass' status = do let (ParseStatus bs pos cls) = status
                            statusOld = ParseStatusOld bs pos []
                        interFacesParsed <- parseClassOld cls statusOld
                        return (curClass interFacesParsed)

parseClassOld :: JC.Class -> ParseStatusOld -> Either (JE.JavaError, ParseStatus) ParseStatus
parseClassOld cls statusOld =
  case parseClassOld' statusOld of
    Left (error, statusOld') -> Left (error, toNewStatus cls statusOld')
    Right statusOld' -> Right (toNewStatus cls statusOld')

parseClassOld' :: ParseStatusOld -> Either (JE.JavaError, ParseStatusOld) ParseStatusOld
parseClassOld' statusOld = do magicParsed <- parseMagicNumber statusOld
                              versionParsed <- parseVersion magicParsed
                              constantPoolParsed <- parseConstantPool versionParsed
                              accessFlagsParsed <- parseAccessFlags constantPoolParsed
                              thisClassParsed <- parseThisClass accessFlagsParsed
                              superClassParsed <- parseSuperClass thisClassParsed
                              parseInterfaces superClassParsed

parseMagicNumber :: ParseStatusOld -> Either (JE.JavaError, ParseStatusOld) ParseStatusOld
parseMagicNumber status@(ParseStatusOld bs byteNum entries) =
  let maybeMagic = readU4 bs
  in  procMagic maybeMagic
  where procMagic Nothing = Left (JE.ClassFileTooShort "Could not read magic number", status)
        procMagic (Just (magic, bs')) | magic == JC.classFormatMagicNumber = Right (ParseStatusOld bs' (byteNum + 4) entries)
                                      | otherwise =  Left (JE.WrongMagicNumber, status)

parseVersion :: ParseStatusOld -> Either (JE.JavaError, ParseStatusOld) ParseStatusOld
parseVersion status@(ParseStatusOld bs byteNum entries) =
  case readNU2 bs 2 of
    Nothing -> Left (JE.ClassFileTooShort "Could not read version numbers", status)
    Just (minor:major:_, bs') -> Right (ParseStatusOld bs' (byteNum + 2)
                                                    (JC.Version minor major:entries))

parseAccessFlags :: ParseStatusOld -> Either (JE.JavaError, ParseStatusOld) ParseStatusOld
parseAccessFlags status@(ParseStatusOld bs byteNum entries) =
  case readU2 bs of
    Nothing -> Left (JE.ClassFileTooShort "Could not read access flags", status)
    Just (accessFlags, bs') -> Right (ParseStatusOld bs' (byteNum + 2)
                                                  (JC.AccessFlags accessFlags:entries))

parseThisClass :: ParseStatusOld -> Either (JE.JavaError, ParseStatusOld) ParseStatusOld
parseThisClass status@(ParseStatusOld bs byteNum entries) =
  case readU2 bs of
    Nothing -> Left (JE.ClassFileTooShort "Could not read this class entry", status)
    Just (thisClass, bs') -> Right (ParseStatusOld bs' (byteNum + 2)
                                                (JC.ThisClass thisClass:entries))

parseSuperClass :: ParseStatusOld -> Either (JE.JavaError, ParseStatusOld) ParseStatusOld
parseSuperClass status@(ParseStatusOld bs byteNum entries) =
  case readU2 bs of
    Nothing -> Left (JE.ClassFileTooShort "Could not read super class entry", status)
    Just (superClass, bs') -> Right (ParseStatusOld bs' (byteNum + 2)
                                                 (JC.SuperClass superClass:entries))

parseInterfaces :: ParseStatusOld -> Either (JE.JavaError, ParseStatusOld) ParseStatusOld
parseInterfaces status@(ParseStatusOld bs byteNum entries) =
  case readInterfaces bs of
    Left error -> Left (error, status)
    Right (interfaces, numInterfaces, bs')
      -> Right (ParseStatusOld bs'
                            (byteNum + (2 * (fromIntegral numInterfaces + 1)))
                            (interfaces:entries))

readInterfaces :: BL.ByteString -> Either JE.JavaError (JC.ClassEntry, U2, BL.ByteString)
readInterfaces bs =
  case readU2 bs of
    Nothing -> Left (JE.ClassFileTooShort "Could not read the interface count")
    Just (numInterfaces, bs') ->
      case readInterfaces' numInterfaces bs' of
        Nothing -> Left (JE.ClassFileTooShort "Could not read the interfaces")
        Just (interfaces, bs'') -> Right (interfaces, numInterfaces, bs'')
        where readInterfaces' num bs'' = do (intArray, bs''') <- readNU2 bs'' (fromIntegral num)
                                            return (JC.Interfaces num intArray, bs''')


