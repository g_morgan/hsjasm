module GMorgan.Java.Class.ParseConstants (parseConstantPool) where

import Data.ByteString.Lazy
import Data.Map
import qualified Data.ByteString                 as SBS

import GMorgan.Java.Class
import GMorgan.Java.Error
import GMorgan.Java.Class.ParseTypes
import GMorgan.Java.Binary.JUtf8
import GMorgan.Java.Class.Binary.Tools


parseConstantPool :: ParseStatusOld -> Either (JavaError, ParseStatusOld) ParseStatusOld
parseConstantPool status@(ParseStatusOld bs byteNum entries) =
  case readU2 bs of
    Nothing -> Left (ClassFileTooShort "Could not read the number of constants", status)
    Just (numConstants, bs') ->
      let status' = ParseStatusOld bs' (byteNum + 2) entries
      in parseConstants status' numConstants

parseConstants :: ParseStatusOld -> U2 -> Either (JavaError, ParseStatusOld) ParseStatusOld
parseConstants (ParseStatusOld bs byteNum entries) max = parseConstants' bs 1 emptyConstantMap 0
  where
    parseConstants' bs' index constMap numBytes
      | index > max = Left (ConstantPoolLargerThanSpecifiedSize,
                            ParseStatusOld bs'
                              (byteNum + numBytes)
                              (CeConstantPool(ConstantPool max constMap):entries))
      | index == max =
          case verifyConstantMap constMap max of
            Nothing -> Right (ParseStatusOld bs'
                              (byteNum + numBytes)
                              (CeConstantPool(ConstantPool max constMap):entries))
            Just e -> Left (e, ParseStatusOld bs'
                              (byteNum + numBytes)
                              (CeConstantPool(ConstantPool max constMap):entries))
      | otherwise = case parseConstantPoolEntry bs' of
          Right (constant, bs'', numBytes') ->
            let constLength = constantLength constant
                newIndex = index + constLength
                newMap = insert index constant constMap
            in parseConstants' bs'' newIndex newMap (numBytes + numBytes')
          Left e -> Left (e, ParseStatusOld bs' (byteNum + numBytes)
                             (CeConstantPool(ConstantPool max constMap):entries))

parseConstantPoolEntry :: ByteString -> Either JavaError (ConstantPoolEntry, ByteString, U8)
parseConstantPoolEntry bs =
  case readU1 bs of
    Nothing -> Left (ClassFileTooShort "Could not read constant tag")
    Just (tag, bs') -> parseConstantPoolEntry' tag bs'
      where
        parseConstantPoolEntry' tag bs'
            | tag == constantUtf8 = parseUtf8Info bs'
            | tag == constantInteger = parseIntegerInfo bs'
            | tag == constantFloat = parseFloatInfo bs'
            | tag == constantLong = parseLongInfo bs'
            | tag == constantDouble = parseDoubleInfo bs'
            | tag == constantClass = parseClassConstant bs'
            | tag == constantStringInfo = parseStringInfo bs'
            | tag == constantFieldRef = parseFieldRef bs'
            | tag == constantMethodRef = parseMethodRef bs'
            | tag == constantInterfaceMethodRef = parseInterfaceMethodRef bs'
            | tag == constantNameAndType = parseNameAndTypeInfo bs'
            | otherwise = Left (UnknownTag tag)

parseUtf8Info :: ByteString -> Either JavaError (ConstantPoolEntry, ByteString, U8)
parseUtf8Info bs =
  case readU2 bs of
    Nothing -> Left (ClassFileTooShort "Could not read UTF constant length")
    Just (strLength, bs') ->
      case readJUtf8 strLength bs' of
        Nothing -> Left (ClassFileTooShort "UTF constant shorter than specified length")
        Just (jutf8, bs'') -> Right (Utf8Info jutf8, bs'', 2 + fromIntegral strLength)



readJUtf8 :: U2 -> ByteString -> Maybe(JUtf8, ByteString)
readJUtf8 x bs = do (vals, newBs) <- readNU1 bs (fromIntegral x)
                    return (decode . SBS.pack $ vals, newBs)


parseIntegerInfo :: ByteString -> Either JavaError (ConstantPoolEntry, ByteString, U8)
parseIntegerInfo bs =
  case readU4 bs of
    Nothing -> Left (ClassFileTooShort "Data shorter than integer")
    Just (int, bs') -> Right (IntegerInfo int, bs', 4)

parseFloatInfo :: ByteString -> Either JavaError (ConstantPoolEntry, ByteString, U8)
parseFloatInfo bs =
  case readU4 bs of
    Nothing -> Left (ClassFileTooShort "Data shorter than float")
    Just (float, bs') -> Right (FloatInfo float, bs', 4)

parseLongInfo :: ByteString -> Either JavaError (ConstantPoolEntry, ByteString, U8)
parseLongInfo bs =
  case readNU4 bs 2 of
    Nothing -> Left (ClassFileTooShort "Data shorter than long")
    Just (high:low:[], bs') -> Right (LongInfo high low, bs', 8)

parseDoubleInfo :: ByteString -> Either JavaError (ConstantPoolEntry, ByteString, U8)
parseDoubleInfo bs =
  case readNU4 bs 2 of
    Nothing -> Left (ClassFileTooShort "Data shorter than double")
    Just (high:low:[], bs') -> Right (DoubleInfo high low, bs', 8)

parseClassConstant :: ByteString -> Either JavaError (ConstantPoolEntry, ByteString, U8)
parseClassConstant bs =
  case readU2 bs of
    Nothing -> Left (ClassFileTooShort "Data shorter than a class constant")
    Just (index, bs') -> Right (ClassConstant index, bs', 2)

parseStringInfo :: ByteString -> Either JavaError (ConstantPoolEntry, ByteString, U8)
parseStringInfo bs =
  case readU2 bs of
    Nothing -> Left (ClassFileTooShort "Data shorter than a string info constant")
    Just (index, bs') -> Right (StringInfo index, bs', 2)

parseFieldRef :: ByteString -> Either JavaError (ConstantPoolEntry, ByteString, U8)
parseFieldRef bs =
  case readNU2 bs 2 of
    Nothing -> Left (ClassFileTooShort "Data shorter than a field ref info constant")
    Just (classIndex:nameIndex:[], bs') -> Right (FieldRef classIndex nameIndex, bs', 4)

parseMethodRef :: ByteString -> Either JavaError (ConstantPoolEntry, ByteString, U8)
parseMethodRef bs =
  case readNU2 bs 2 of
    Nothing -> Left (ClassFileTooShort "Data shorter than a method ref info constant")
    Just (classIndex:nameIndex:[], bs') -> Right (MethodRef classIndex nameIndex, bs', 4)

parseInterfaceMethodRef :: ByteString -> Either JavaError (ConstantPoolEntry, ByteString, U8)
parseInterfaceMethodRef bs =
  case readNU2 bs 2 of
    Nothing -> Left (ClassFileTooShort "Data shorter than an interface method ref info constant")
    Just (classIndex:nameIndex:[], bs') -> Right (InterfaceMethodRef classIndex nameIndex, bs', 4)

parseNameAndTypeInfo :: ByteString -> Either JavaError (ConstantPoolEntry, ByteString, U8)
parseNameAndTypeInfo bs =
  case readNU2 bs 2 of
    Nothing -> Left (ClassFileTooShort "Data shorter than a name and type info constant")
    Just (nameIndex:descrIndex:[], bs') -> Right (NameAndTypeInfo nameIndex descrIndex, bs', 4)