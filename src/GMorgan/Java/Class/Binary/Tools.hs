module GMorgan.Java.Class.Binary.Tools where

import Data.ByteString.Lazy hiding (foldl, reverse)
import Data.Word

type U1 = Word8
type U2 = Word16
type U4 = Word32
type U8 = Word64

readU1 :: ByteString -> Maybe(U1, ByteString)
readU1 = uncons

readU2 :: ByteString -> Maybe (U2, ByteString)
readU2 bs = readUN bs 2

readU4 :: ByteString -> Maybe (U4, ByteString)
readU4 bs = readUN bs 4

readUN :: (Integral a) => ByteString -> Int -> Maybe(a, ByteString)
readUN bs n = do (vals, newBs) <- readNU1 bs n
                 return (foldl accum 0 vals, newBs)
                 where
                   accum val cur = val * 256 + fromIntegral cur

readNU1 :: ByteString -> Int -> Maybe ([U1], ByteString)
readNU1 = readNVal readU1

readNU2 :: ByteString -> Int -> Maybe ([U2], ByteString)
readNU2 = readNVal readU2

readNU4 :: ByteString -> Int -> Maybe ([U4], ByteString)
readNU4 = readNVal readU4

readNVal :: (ByteString -> Maybe (a, ByteString)) -> ByteString -> Int ->
            Maybe ([a], ByteString)
readNVal func = readNValRec func []

readNValRec :: (ByteString -> Maybe (a, ByteString)) -> [a] -> ByteString ->
               Int -> Maybe ([a], ByteString)
readNValRec _ lst bs 0 = Just (reverse lst, bs)
readNValRec func lst bs n
  | n < 0 = Nothing
  | otherwise = do (val, newBs) <- func bs 
                   readNValRec func (val : lst) newBs (n - 1)
