module GMorgan.Java.Class.ParseTypes where
import qualified Data.ByteString.Lazy            as BL
import qualified GMorgan.Java.Class              as JC
import GMorgan.Java.Class.Binary.Tools

data ParseStatusOld = ParseStatusOld BL.ByteString U8 [JC.ClassEntry] deriving Show

data ParseStatus =
  ParseStatus
  {
    bytesRemaining :: BL.ByteString,
    bytesParsed :: U8,
    curClass :: JC.Class
  }
  deriving Show

toNewStatus ::  JC.Class -> ParseStatusOld -> ParseStatus
toNewStatus cls status@(ParseStatusOld bs byteNum entries) = ParseStatus bs byteNum (JC.mergeClassEntries cls entries)
