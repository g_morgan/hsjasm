module GMorgan.Java.Parsing.ParseAttributes where

import Data.ByteString.Lazy

import GMorgan.Java.AttributeInfo
import GMorgan.Java.Error
import GMorgan.Java.Class
import GMorgan.Java.Class.Binary.Tools

parseAttribute :: ConstantPool -> ByteString -> Either JavaError (AttributeInfo, ByteString)
parseAttribute constants bs =
  case readU2 bs of
    Nothing -> Left (ClassFileTooShort "Could not read attribute_name_index")
    Just (nameIndex, bs') ->
      case getConstantFromPool nameIndex constants of
        Nothing -> Left (BadAttributeConstant "No constant with this index")
        Just constant -> parseAttribute' nameIndex constant bs'

parseAttribute' :: U2 -> ConstantPoolEntry -> ByteString -> Either JavaError (AttributeInfo, ByteString)
parseAttribute' nameIndex constant bs =
  case constant of
    Utf8Info _ -> parseGeneralAttribute nameIndex bs -- add case statement here when we have more attribute types
    otherwise -> Left (BadAttributeConstant $ "Expected a constant of type Utf8Info, actually got " ++ typeName constant)

parseGeneralAttribute :: U2 -> ByteString -> Either JavaError (AttributeInfo, ByteString)
parseGeneralAttribute nameIndex bs =
  case readU4 bs of
    Nothing -> Left (ClassFileTooShort "Could not read attribute_length")
    Just (attributeLength, bs') ->
      case readNU1 bs' (fromIntegral attributeLength) of
        Nothing -> Left (ClassFileTooShort "Could not read attribute info")
        Just (info, bs'') -> Right (GeneralAttribute nameIndex attributeLength info, bs'')

