module Main where
import qualified Data.ByteString.Lazy as BL
import qualified GMorgan.Java.Class.Parse as JCP
import System.Environment
import qualified GMorgan.Java.Class.Binary.Tools as JCBT
main :: IO ()
main = do
  args <- System.Environment.getArgs
  let filename = head args
  clazz <- JCP.readClass filename
  print clazz
